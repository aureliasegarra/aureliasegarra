# Hi there 👋, I’m Aurelia !


* 🧪 I'm currently a Software QA tester


<br>

### 📊 Project management
* Working in **Agile method**
* Transmitting bugs report in **Jira**
* Writing and updating reference documents in **Confluence**

### 🚀 Test management
* Using **XRay** plugin in **Jira** for test management

### 🤖 Automation
* Automate non-regression testing with **Cypress**
* Automate End-to-End with **RobotFramework**
* Precious help with the web driver **Selenium** 

### ⚙️ Languages
* Javascript
* Python
* Java


## Roadmap 2024
* Playwright
* API Testing
* TestComplete

## 💬 Want to get in touch ?  

* Find me on Linkedin <a href="https://www.linkedin.com/in/aureliasegarra/">here</a>
* Find me on Github <a href="https://github.com/aureliasegarra">here</a>
* Leave me a message  👉 aureliasegarra@protonmail.com


<br>
<br>
<br>
<br>

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)


